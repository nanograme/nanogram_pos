import 'package:flutter/material.dart';
import 'package:nanograme_pos/components/custom_numpad.dart';
import 'package:flutter/services.dart';

class NumpadScreen extends StatefulWidget{
  @override
  _NumpadScreenState createState() => _NumpadScreenState();

}

class _NumpadScreenState extends State<NumpadScreen> {

  int length = 4 ;
  onChange(String number){
    if(number.length == length){
      Navigator.pushReplacementNamed(context, '/index-page');
    }
    print(number);
  }

  Widget _buildPin(){
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 0.0), //กรณีไม่มี 15
            child: Text(
            'Please Input your PIN Code.',
            style: TextStyle(
                fontSize: 20.0,
                color: Colors.white,
                fontWeight: FontWeight.w400,
            ),
          ),
          ),
          Numpad(length: length,onChange: onChange,)
        ],
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color(0xFF73AEF5),
                      Color(0xFF61A4F1),
                      Color(0xFF478DE0),
                      Color(0xFF398AE5),
                    ],
                    stops: [0.1, 0.4, 0.7, 0.9],
                  ),
                ),
              ),
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 10.0,
                    vertical: 120.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _buildPin()
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

