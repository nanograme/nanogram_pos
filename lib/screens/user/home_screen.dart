import 'package:flutter/material.dart';
import 'package:nanograme_pos/screens/user/index_screen.dart';
//import 'package:nanograme_pos/screens/user/checkin_screen.dart';
import 'package:nanograme_pos/screens/user/contact_screen.dart';

void main() {
  runApp(HomeScreen());
}

class HomeScreen extends StatelessWidget {
  // static const String _title = 'Flutter';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // title: _title,
      home: MyStatefulWidget(),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {

  int _currentIndex = 1;
  final List<Widget> _children = [
    IndexScreen(),
    //CheckinScreen(),
    ContactScreen(),
  ];
  void _onTappedBar(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
  //---------- จบ Navigation Bottom Tab ---------

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:_children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _onTappedBar,
        currentIndex: _currentIndex,
        //backgroundColor: Colors.deepOrange,
        unselectedItemColor: Colors.black45,
        selectedItemColor: Colors.deepOrange,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.place_sharp),
            label: 'Check-in',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.supervised_user_circle),
            label: 'Account',
          ),
        ],
        // selectedItemColor: Colors.amber[800],
      ),
    );
  }
}
