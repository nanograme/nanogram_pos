import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:nanograme_pos/components/custom_tabbar.dart';
//import 'package:nanograme_pos/screens/user/checkin_screen.dart';
import 'package:nanograme_pos/screens/user/contact_screen.dart';
import 'package:nanograme_pos/components/custom_shape_cliper.dart';
import 'package:nanograme_pos/screens/user/loading_test.dart';
import 'package:nanograme_pos/components/custom_drawer.dart';
import 'package:flutter/services.dart';
import 'package:nanograme_pos/components/custom_tabscreen.dart';

class IndexScreen extends StatefulWidget{

  //tab bar
  IndexScreen({Key key, this.title}) : super(key: key);
  final String title;
  //_______
  @override 
  _IndexScreenState createState() => _IndexScreenState();
}

class _IndexScreenState extends State<IndexScreen> with TickerProviderStateMixin {
  //tab bar
  TabController _controller;
  @override
  void initState() {
    super.initState();
    _controller = new TabController(length: 3, vsync: this,
    );
  }
  @override
  void dispose() {
    super.dispose();

  }
  //------------
  @override 
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: const Text("NANOGRAM POS"),
        actions: [
          IconButton(icon: Icon(Icons.search), onPressed: (){})
        ],

        bottom: new TabBar(
          controller: _controller,
          indicatorSize: TabBarIndicatorSize.tab,
          labelColor: Color(0xFFFFFFFF),
          tabs: const <Tab>[
            const Tab(text: 'เมนูที่ 1'),
            const Tab(text: 'เมนูที่ 2'),
            const Tab(text: 'เมนูที่ 3'),
          ],
        ),

      ),
      drawer: Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text('Devalonez'),
            accountEmail: Text('usman.devalone@gmail.com'),
            currentAccountPicture: CircleAvatar(
              child: FlutterLogo(size: 42.0,),
              backgroundColor: Colors.white,
            ),
            otherAccountsPictures: <Widget>[
              CircleAvatar(
                child: Text("N"),
                foregroundColor: Colors.white,
                backgroundColor: Colors.orange,
              ),
              CircleAvatar(
                child: Icon(Icons.add),
                foregroundColor: Colors.white,
                backgroundColor: Colors.grey,
              )
            ],
          ),
          ListTile(
              leading: Icon(FontAwesomeIcons.copy,color: Colors.blue,),
              title: Text('ออเดอร์'),
              onTap: () {}
          ),
          ListTile(
            leading: Icon(FontAwesomeIcons.user,color: Colors.blue,),
            title: Text('โปรไฟล์'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(FontAwesomeIcons.mapMarkerAlt,color: Colors.blue,),
            title: Text('ที่อยู่'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(FontAwesomeIcons.questionCircle,color: Colors.blue,),
            title: Text('ศูนย์ช่วยเหลือ'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(FontAwesomeIcons.cog,color: Colors.blue,),
            title: Text('ตั้งค่า'),
            onTap: () {},
          ),
          Divider(),
          Expanded(
            child: Align(
              alignment: Alignment.bottomLeft,
              child: ListTile(
                leading: Icon(FontAwesomeIcons.signOutAlt,color: Colors.blue,),
                title: Text('ออกจากระบบ'),
                onTap: () {},
              ),
            ),
          ),
        ],
      ),

      ),

      body: new TabBarView(
        controller: _controller,
        children: <Widget>[
          new TabScreen("เมนู 1"),
          new TabScreen("เมนู 2"),
          new TabScreen("เมนูที่ 3"),
        ],
      ),
    );
  }

}