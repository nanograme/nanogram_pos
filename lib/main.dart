import 'package:flutter/material.dart';
import 'package:nanograme_pos/screens/login_pin_screen.dart';
import 'package:nanograme_pos/screens/login_screen.dart';
import 'package:nanograme_pos/screens/user/contact_screen.dart';
import 'package:nanograme_pos/screens/user/index_screen.dart';
import 'package:nanograme_pos/screens/user/loading_test.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Nanograme POS APP',
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
      routes: {
        '/pin-page': (context) => NumpadScreen(),
        '/index-page':(context) => IndexScreen(),
        '/loading-page':(context) => ColorLoader3(),
      },
    );
  }
}


