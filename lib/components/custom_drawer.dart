
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget _CustomDrawer(){
  return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: Text('Usman Laemae'),
          accountEmail: Text("usman.laemae@tcpinternetwork.com"),
          currentAccountPicture: CircleAvatar(
            child: FlutterLogo(size: 42.0,),
            backgroundColor: Colors.white,
          ),
          otherAccountsPictures: <Widget>[
            CircleAvatar(
              child: Text('M'),
              foregroundColor: Colors.white,
              backgroundColor: Colors.orange,
            ),
            CircleAvatar(
              child: Text('S'),
              foregroundColor: Colors.white,
              backgroundColor: Colors.orange,
            )
          ],
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.copy,color: Colors.blue,),
          title: Text("ออเดอร์"),
          onTap: (){},
        ),
        //  Divider(),// line
        ListTile(
          leading: Icon(FontAwesomeIcons.user,color: Colors.blue,),
          title: Text('โปรไฟล์'),
          onTap: (){},
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.mapMarkerAlt,color: Colors.blue,),
          title: Text('ที่อยู่'),
          onTap: (){},
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.questionCircle,color: Colors.blue,),
          title: Text('ศูนย์ช่วยเหลือ'),
          onTap: (){},
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.cog,color: Colors.blue,),
          title: Text('ตั้งค่า'),
          onTap: (){},
        ),
        Divider(),

        Expanded(
          child: Align(
              alignment: Alignment.bottomLeft,
              child: ListTile(
                leading: Icon(FontAwesomeIcons.signOutAlt,color: Colors.blue,),
                title: Text("ออกจากระบบ"),
                onTap: (){
                  // Navigator.popUntil(context, '/loading');
                },
              )

          ),
        ),
      ],

  );
}