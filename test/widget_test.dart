import 'package:flutter/material.dart';
import 'package:nanograme_pos/main.dart';
import 'package:nanograme_pos/screens/login_screen.dart';

void main() {
  runApp(MyApp());
}

  class MyApp extends StatelessWidget{
    @override
    Widget build (BuildContext context){
      return MaterialApp(
        title: '',
        debugShowCheckedModeBanner: false,
        home:LoginScreen(),
      );
    }
  }

